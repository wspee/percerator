document.addEventListener('DOMContentLoaded', () => {

  chrome.storage.sync.get(null, (data) =>
  {
    console.log('DOMContentLoaded', data);
    if (data && data.abpExtensionId)
      document.querySelector("#abp-extension-id").value = data.abpExtensionId;
    if (data && data.blockBits)
      document.querySelector("#block-bits").value = data.blockBits;
    else
      document.querySelector("#block-bits").value = 8;
  });


  document.querySelector('#save').addEventListener("click", (event) =>
  {
    let extensionId = document.querySelector("#abp-extension-id").value;
    let blockBits = parseInt(document.querySelector("#block-bits").value, 10);
    chrome.storage.sync.set({
      abpExtensionId: extensionId,
      blockBits: blockBits,
    });
    chrome.runtime.sendMessage({action: "refreshSettings"});
  });

});
